import argparse
import time
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.utils.MCMC_mv import update_mv, update_all_mv


parser = argparse.ArgumentParser(description='Generating Multi-dimensional Distribution Files')
parser.add_argument('--overwrite', action='store_true',
                    help='Whether to overwrite existing file')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str,
                    help='Term `@id` from Hestia glossary')
parser.add_argument('--sample-size', type=int, default=30,
                    help='Number of samples on the each sample grid. Default 30')
args = parser.parse_args()


def main():
    start_time = time.time()

    if args.product_id:
        update_mv(args.country_id, args.product_id, args.sample_size)
    else:
        update_all_mv(args.country_id, args.sample_size, overwrite=args.overwrite)

    print(f"Finished. Total time: {int(time.time() - start_time)} secs.")


if __name__ == "__main__":
    main()
