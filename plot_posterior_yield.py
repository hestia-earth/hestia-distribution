### Usage
### pip install arviz
import argparse
import arviz as az
import matplotlib.pyplot as plt
from dotenv import load_dotenv


load_dotenv()


from hestia_earth.distribution.log import logger
from hestia_earth.distribution.utils import get_stats_from_df
from hestia_earth.distribution.posterior_yield import get_post_ensemble
from hestia_earth.distribution.prior_yield import generate_prior_yield_file
from hestia_earth.distribution.likelihood import generate_likl_file
from hestia_earth.distribution.cycle import YIELD_COLUMN



parser = argparse.ArgumentParser(description='Plot Yield Posterior figures')
parser.add_argument('--output-file', type=str, default='posterior.png',
                    help='Filepath to save the result to as PNG')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str, required=True,
                    help='Product term `@id` from Hestia glossary')
args = parser.parse_args()


def plot_posterior(mu_ens: list, mu_prior: float, mu_obs: float):
    fig, ax = plt.subplots()
    # sns.distplot(idata['posterior_predictive']['obs'].mean(axis=1), label='Posterior predictive means', ax=ax)
    az.plot_dist(mu_ens, fill_kwargs={'alpha': 0.3}, label='Posteriors')

    ax.axvline(mu_obs, ls='--', color='r', label='Obs. mean')
    ax.axvline(mu_prior, ls='--', color='g', label='Prior mean', alpha=0.5)
    # plt.xlim(0, 10000)

    ax.set_title(f"{args.country_id.split('-')[-1]} {args.product_id} Yield Posterior Distribution")
    ax.legend(loc=2)

    plt.savefig(args.output_file)
    logger.info(f'Figure saved as {args.output_file}')


def main():
    df_prior = generate_prior_yield_file()
    mu_prior, _sigma_prior = get_stats_from_df(df_prior, args.country_id, args.product_id)
    df_likl = generate_likl_file(args.country_id, args.product_id, overwrite=False)

    mu_ens, _ = get_post_ensemble(args.country_id, args.product_id)
    plot_posterior(mu_ens, mu_prior, df_likl[YIELD_COLUMN].mean())


if __name__ == "__main__":
    main()
