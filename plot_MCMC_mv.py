### Usage
### pip install seaborn
import argparse
import numpy as np
import matplotlib.pyplot as plt
from dotenv import load_dotenv
from math import ceil


load_dotenv()


from hestia_earth.distribution.cycle import cycle_yield_distribution
from hestia_earth.distribution.utils.cycle import (
    YIELD_COLUMN, FERTILISER_COLUMNS, PESTICIDE_COLUMN, IRRIGATION_COLUMN,
    _FERT_GROUPS
)
from hestia_earth.distribution.utils.MCMC_mv import (
    _fit_user_data_mv, _get_df_bounds, read_mv, _mv_filename, MV_COLUMNS,
    find_likelihood_from_static_file
)
from hestia_earth.distribution.likelihood import generate_likl_file
from hestia_earth.distribution.posterior_yield import get_post_ensemble as get_yield_ensemble
from hestia_earth.distribution.posterior_fert import get_post_ensemble as get_fert_ensemble
from hestia_earth.distribution.posterior_pest import get_post_ensemble as get_pest_ensemble
from hestia_earth.distribution.posterior_irrigation import get_post_ensemble as get_irri_ensemble


parser = argparse.ArgumentParser()
parser.add_argument('--output', type=str, default='yield_vs_n.png',
                    help='Filepath to save the result to as PNG.')
parser.add_argument('--y', type=int, required=True,
                    help='Candidate grain yield value (kg/ha) to be tested.')
parser.add_argument('--n', type=int, default=None,
                    help='Candidate Nitrogen usage (kgN/ha) to be tested.')
parser.add_argument('--p', type=int, default=None,
                    help='Candidate Phosphorus usage (kgP2O5/ha) to be tested.')
parser.add_argument('--k', type=int, default=None,
                    help='Candidate Potassium usage (kgK2O/ha) to be tested.')
parser.add_argument('--pest', type=float, default=None,
                    help='Total pesticide usage (kg active ingredient / ha) to be tested.')
parser.add_argument('--water', type=float, default=None,
                    help='Total irrigation water usage (m3 / ha) to be tested.')
parser.add_argument('--country-id', type=str, required=True,
                    help='Region `@id` from Hestia glossary')
parser.add_argument('--product-id', type=str, required=True,
                    help='Product term `@id` from Hestia glossary')
parser.add_argument('--threshold', type=float, default=0.95,
                    help='Percentile threshold to be accepted')
parser.add_argument('--background', type=bool, default=False,
                    help='Add posterior ensembles to the univariate distributions as background data')
args = parser.parse_args()


def main():
    inputs = {
        FERTILISER_COLUMNS[0]: args.n,
        FERTILISER_COLUMNS[1]: args.p,
        FERTILISER_COLUMNS[2]: args.k,
        PESTICIDE_COLUMN: args.pest,  # if give, on-demand mv calculation will be triggered
        IRRIGATION_COLUMN: args.water  # if give, on-demand mv calculation will be triggered
    }

    inputs = {k: v for k, v in inputs.items() if v is not None}

    if args.pest != None or args.water != None:
        plot_multivariate_on_demand(list(inputs.values())+[args.y], args.country_id, args.product_id,
                   list(inputs.keys())+[YIELD_COLUMN],
                   args.output, args.threshold, args.background)
    else:
        plot_multivariate_static(list(inputs.values())+[args.y], args.country_id, args.product_id,
                   args.output, args.threshold)


def plot_multivariate_static(candidate: list, country_id: str, product_id: str,
                   output_file: str, threshold: float = 0.95):

    likl, loc = find_likelihood_from_static_file(candidate, country_id, product_id)
    plt.imshow(likl[:][loc[1]][loc[2]][:])  # plot N vs Yield 
    plt.plot(candidate[3], candidate[0], color='r')


def plot_multivariate_on_demand(candidate: list, country_id: str, product_id: str, columns: list,
                   output_file: str, threshold: float = 0.95, use_posterior_background: bool = False):
    df = generate_likl_file(country_id, product_id)

    if use_posterior_background:
        columns = FERTILISER_COLUMNS[:3] + [PESTICIDE_COLUMN, YIELD_COLUMN]
        df_background = cycle_yield_distribution({})
        
        df_background[YIELD_COLUMN] = np.concatenate(get_yield_ensemble(country_id, product_id)).flatten()
        for input_id in FERTILISER_COLUMNS[:3]:
            fert_id = _FERT_GROUPS[input_id.strip(')').split('kg ')[-1]]
            df_background[input_id] = np.concatenate(get_fert_ensemble(country_id, product_id, fert_id)).flatten()
        df_background[PESTICIDE_COLUMN] = np.concatenate(get_pest_ensemble(country_id, product_id)).flatten()

    likelihood, Z = _fit_user_data_mv(candidate, df, columns, return_z=False)

    if likelihood != None:
        m, mins, maxs = _get_df_bounds(df, columns)

        # plot multiple univariate distributions
        fig, ax = plt.subplots(2, ceil((len(m)+1)/2))
        ax = ax.flatten()

        for i in range(len(m)):
            #data = df_background[columns[i]] if use_posterior_background else m[i]

            ax[i].hist(m[i])

            if use_posterior_background:
                ax2 = ax[i].twinx()
                #sns.kdeplot(df_background[columns[i]], ax=ax2, color='c')
                ax2.hist(df_background[columns[i]], alpha=0.3)

            ax[i].axvline(x = candidate[i], color='r')
            ax[i].set_title(columns[i])

        # Always plot bi-variate between yield and N fertiliser as the last subplot
        candidate_2d = [candidate[0], candidate[-1]]
        columns_2d = [columns[0], columns[-1]]
        likelihood_2d, Z_2d = _fit_user_data_mv(candidate_2d, df, columns_2d, return_z=True)
        m_2d, mins_2d, maxd_2d = _get_df_bounds(df, columns_2d)
        ax[-1].imshow(np.rot90(Z_2d), cmap=plt.cm.gist_earth_r, aspect='auto',
                        extent=[mins_2d[0], maxd_2d[0], mins_2d[1], maxd_2d[1]])
        ax[-1].plot(m_2d[0], m_2d[1], 'k.', markersize=1)
        ax[-1].set_xlim([mins_2d[0], maxd_2d[0]])
        ax[-1].set_ylim([mins_2d[1], maxd_2d[1]])
        ax[-1].set_xlabel(columns_2d[0])
        ax[-1].set_ylabel(columns_2d[1])

        # plot candidate
        marker = 'ro' if likelihood < 1 - threshold else 'co'
        ax[-1].plot(candidate_2d[0], candidate_2d[1], marker)
        ax[-1].set_title(f'Likelihood={likelihood:.1%}')

        plt.gca().set_aspect('auto', adjustable='datalim')

        plt.tight_layout()
        plt.savefig(output_file, format='png')
    return likelihood


if __name__ == "__main__":
    main()
